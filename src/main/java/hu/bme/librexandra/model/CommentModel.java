package hu.bme.librexandra.model;

import java.time.Instant;

public class CommentModel {

    private Instant timestamp;
    private String text;

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
