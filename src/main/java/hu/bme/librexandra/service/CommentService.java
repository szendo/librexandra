package hu.bme.librexandra.service;

import hu.bme.librexandra.entity.Comment;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface CommentService {

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void setStatus(Long id, Comment.Status status);

    @PreAuthorize("hasRole('ROLE_USER')")
    void reportComment(Long id);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    List<Comment> getReportedComments();

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    long countReported();

    @PreAuthorize("hasRole('ROLE_USER')")
    Comment findById(Long id);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void deleteComment(Long id);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void editComment(Long id, String text);

}
