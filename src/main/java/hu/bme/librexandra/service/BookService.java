package hu.bme.librexandra.service;

import hu.bme.librexandra.entity.Book;
import hu.bme.librexandra.model.BookModel;
import hu.bme.librexandra.model.CommentModel;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface BookService {

    int COVER_WIDTH = 330;
    int COVER_HEIGHT = 440;

    List<Book> listAccepted();

    Page<Book> listAccepted(int page, int size);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    List<Book> listPending();

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    long countPending();

    @PreAuthorize("hasRole('ROLE_USER')")
    Book create(BookModel bookModel);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void setStatus(Long id, Book.Status status);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void delete(Long id);

    @PreAuthorize("hasRole('ROLE_USER')")
    void addComent(Long id, CommentModel commentModel);

    @PreAuthorize("hasRole('ROLE_USER')")
    void rateBook(Long id, Short value);

    Book findById(Long id);

    byte[] getCoverById(Long id);

}
