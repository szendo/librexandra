package hu.bme.librexandra.service.impl;

import hu.bme.librexandra.entity.Book;
import hu.bme.librexandra.entity.Comment;
import hu.bme.librexandra.entity.Rating;
import hu.bme.librexandra.entity.User;
import hu.bme.librexandra.model.BookModel;
import hu.bme.librexandra.model.CommentModel;
import hu.bme.librexandra.repository.BookRepository;
import hu.bme.librexandra.repository.RatingRepository;
import hu.bme.librexandra.service.BookService;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private RatingRepository ratingRepository;


    @Override
    public List<Book> listAccepted() {
        return bookRepository.findByStatusOrderByTitleAsc(Book.Status.ACCEPTED);
    }

    @Override
    public Page<Book> listAccepted(int page, int size) {
        return bookRepository.findByStatusOrderByTitleAsc(Book.Status.ACCEPTED, new PageRequest(page, size));
    }

    @Override
    public List<Book> listPending() {
        return bookRepository.findByStatusOrderByTitleAsc(Book.Status.PENDING);
    }

    public long countPending() {
        return bookRepository.countPending();
    }

    @Override
    public Book create(BookModel bookModel) {
        final Book book = new Book();
        book.setTitle(bookModel.getTitle());
        book.setAuthor(bookModel.getAuthor());
        book.setDescription(bookModel.getDescription());
        book.setYear(bookModel.getYear());
        book.setStatus(Book.Status.PENDING);
        try (InputStream is = bookModel.getCover().getInputStream()) {
            final BufferedImage unscaled = ImageIO.read(is);
            if (unscaled == null) {
                logger.info("Invalid cover image uploaded");
                throw new RuntimeException("Invalid cover image uploaded");
            }
            final BufferedImage scaled = Scalr.resize(unscaled, COVER_WIDTH, COVER_HEIGHT);
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                ImageIO.write(scaled, "png", baos);
                book.setCover(baos.toByteArray());
            }
        } catch (IOException e) {
            logger.warn("Got exception while processing cover image", e);
            throw new RuntimeException("Got exception while processing cover image", e);
        }
        return bookRepository.save(book);
    }

    @Override
    public void setStatus(Long id, Book.Status status) {
        final Book book = bookRepository.findOne(id);
        if (book == null) {
            logger.warn("Book with id {} not found", id);
        } else {
            book.setStatus(status);
            bookRepository.save(book);
        }

    }

    @Override
    public void delete(Long id) {
        bookRepository.delete(id);
    }

    @Override
    public void addComent(Long id, CommentModel commentModel) {
        final Book book = bookRepository.findOne(id);
        if (book == null) {
            logger.warn("Book with id {} not found", id);
        } else {
            final Comment comment = new Comment();
            comment.setUser((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
            comment.setBook(book);
            comment.setTimestamp(commentModel.getTimestamp());
            comment.setText(commentModel.getText());
            comment.setStatus(Comment.Status.ORIGINAL);
            bookRepository.save(book);
        }
    }

    @Override
    public void rateBook(Long id, Short value) {
        final User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final Rating existingRating = ratingRepository.getRating(id, user.getId());
        if (existingRating != null) {
            existingRating.setValue(value);
            ratingRepository.save(existingRating);
        } else {
            final Book book = bookRepository.findOne(id);
            if (book == null) {
                logger.warn("Book with id {} not found", id);
            } else {
                final Rating rating = new Rating();
                rating.setUser(user);
                rating.setBook(book);
                rating.setValue(value);
                ratingRepository.save(rating);
            }
        }
    }

    @Override
    public Book findById(Long id) {
        Book book = bookRepository.findOne(id);
        Double score = ratingRepository.getBookScore(id);
        if (score == null) {
            score = 0.0;
        }
        book.setScore(score);
        return book;
    }

    @Override
    public byte[] getCoverById(Long id) {
        return bookRepository.loadCoverById(id);
    }


}
