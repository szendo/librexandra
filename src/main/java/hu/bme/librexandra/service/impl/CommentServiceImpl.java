package hu.bme.librexandra.service.impl;


import hu.bme.librexandra.entity.Comment;
import hu.bme.librexandra.repository.CommentRepository;
import hu.bme.librexandra.service.CommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    CommentRepository commentRepository;

    @Override
    public void setStatus(Long id, Comment.Status status) {
        final Comment comment = commentRepository.getOne(id);
        if (comment == null) {
            logger.warn("Comment with id {} not found", id);
        } else {
            comment.setStatus(status);
            commentRepository.save(comment);
        }
    }

    @Override
    public void reportComment(Long id) {
        final Comment comment = commentRepository.getOne(id);
        if (comment == null) {
            logger.warn("Comment with id {} not found", id);
        } else if (comment.getStatus() != Comment.Status.ORIGINAL) {
            throw new RuntimeException("Comment cannot be reported, status isn't ORIGINAL");
        } else {
            comment.setStatus(Comment.Status.REPORTED);
            commentRepository.save(comment);
        }

    }

    @Override
    public void editComment(Long id, String text) {
        final Comment comment = commentRepository.getOne(id);
        if (comment == null) {
            logger.warn("Comment with id {} not found", id);
        } else {
            comment.setStatus(Comment.Status.EDITED);
            comment.setText(text);
            commentRepository.save(comment);
        }
    }

    @Override
    public List<Comment> getReportedComments() {
        return commentRepository.getReportedComments();
    }

    @Override
    public long countReported() {
        return commentRepository.countReported();
    }

    @Override
    public Comment findById(Long id) {
        return commentRepository.getOne(id);
    }

    @Override
    public void deleteComment(Long id) {
        commentRepository.deleteComment(id);
    }


}
