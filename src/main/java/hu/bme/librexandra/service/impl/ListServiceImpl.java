package hu.bme.librexandra.service.impl;

import hu.bme.librexandra.entity.Book;
import hu.bme.librexandra.entity.BookList;
import hu.bme.librexandra.entity.BookListEntry;
import hu.bme.librexandra.entity.User;
import hu.bme.librexandra.exception.AuthorizationException;
import hu.bme.librexandra.model.ListModel;
import hu.bme.librexandra.repository.BookRepository;
import hu.bme.librexandra.repository.ListRepository;
import hu.bme.librexandra.service.ListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ListServiceImpl implements ListService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ListRepository listRepository;

    @Autowired
    private BookRepository bookRepository;

    @Override
    public BookList create(ListModel listModel) {
        final BookList list = new BookList();
        list.setName(listModel.getName());
        list.setUser((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        list.setTimestamp(Instant.now());
        return listRepository.save(list);
    }

    @Override
    public BookList findById(Long id) {
        return listRepository.findOne(id);
    }

    @Override
    public List<BookList> listByCurrentUser() {
        return listRepository.findByUserOrderByTimestampAsc((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    @Override
    public List<BookList> listByCurrentUserForBook(Long bookId) {
        final User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (currentUser == null) {
            return null;
        }
        return listRepository.findByUserOrderByTimestampAsc(currentUser).stream()
                .filter(l -> new ArrayList<>(l.getEntries()).stream().noneMatch(e -> e.getBook().getId().equals(bookId)))
                .collect(Collectors.toList());
    }

    @Override
    public List<BookList> listForBook(Long id) {
        final Book book = bookRepository.getOne(id);
        if (book == null) {
            logger.warn("Book with id {} not found", id);
            throw new RuntimeException("Cannot find list for nonexistent book.");
        }
        return listRepository.findForBook(book).stream()
                .limit(PUBLIC_LIST_LIMIT).collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        final User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final BookList list = listRepository.findOne(id);
        if (!currentUser.getId().equals(list.getUser().getId())) {
            logger.info("{} ({}) tried to delete list owned by someone else.", currentUser.getUsername(), currentUser.getId());
            throw new AuthorizationException("You can only delete your own lists.");
        }
        listRepository.delete(list);
    }

    @Override
    public void addEntry(Long id, Long bookId) {
        final User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final BookList list = listRepository.findOne(id);
        if (!currentUser.getId().equals(list.getUser().getId())) {
            logger.info("{} ({}) tried to modify list owned by someone else.", currentUser.getUsername(), currentUser.getId());
            throw new AuthorizationException("You can only modify your own lists.");
        }
        final Book book = bookRepository.getOne(bookId);
        if (book == null) {
            logger.warn("Book with id {} not found", id);
            throw new RuntimeException("Cannot add nonexistent book to list.");
        }
        final BookListEntry entry = new BookListEntry();
        entry.setList(list);
        entry.setBook(book);
        entry.setTimestamp(Instant.now());
        list.addEntry(entry);
        listRepository.save(list);
    }

    @Override
    public void removeEntry(Long id, Long bookId) {
        final User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final BookList list = listRepository.findOne(id);
        if (!currentUser.getId().equals(list.getUser().getId())) {
            logger.info("{} ({}) tried to modify list owned by someone else.", currentUser.getUsername(), currentUser.getId());
            throw new AuthorizationException("You can only modify your own lists.");
        }
        final Book book = bookRepository.getOne(bookId);
        if (book == null) {
            logger.warn("Book with id {} not found", id);
            throw new RuntimeException("Cannot remove nonexistent book from list.");
        }
        final BookListEntry entry = listRepository.findEntryInList(list, book);
        if (entry == null) {
            logger.info("Book #{} is not on list #{}", bookId, id);
            throw new RuntimeException("Book is not on list");
        }
        list.removeEntry(entry);
        listRepository.save(list);
    }

}
