package hu.bme.librexandra.service.impl;

import hu.bme.librexandra.entity.User;
import hu.bme.librexandra.exception.ApplicationAlreadyConfiguredException;
import hu.bme.librexandra.model.UserModel;
import hu.bme.librexandra.repository.UserRepository;
import hu.bme.librexandra.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User create(UserModel userModel) {
        final User user = new User();
        user.setUsername(userModel.getUsername());
        user.setPassword(passwordEncoder.encode(userModel.getPassword()));
        user.setEmail(userModel.getEmail());
        user.setStatus(User.Status.USER);

        return userRepository.save(user);
    }

    @Override
    public User createFirst(UserModel userModel) {
        if (userRepository.count() > 0) {
            logger.warn("Initial user already exists.");
            throw new ApplicationAlreadyConfiguredException();
        }

        final User user = new User();
        user.setUsername(userModel.getUsername());
        user.setPassword(passwordEncoder.encode(userModel.getPassword()));
        user.setEmail(userModel.getEmail());
        user.setStatus(User.Status.ADMIN);

        return userRepository.save(user);
    }

    @Override
    public void setStatus(Long id, User.Status status) {
        final User user = userRepository.findOne(id);
        if (user == null) {
            logger.warn("User with id {} not found", id);
        } else {
            user.setStatus(status);
            userRepository.save(user);
        }
    }

    @Override
    public long count() {
        return userRepository.count();
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User userEntity = userRepository.findByUsername(username);
        if (userEntity != null) {
            return userEntity;
        }
        throw new UsernameNotFoundException("");
    }

    @Override
    public List<User> listUsers() {
        return userRepository.findAll();
    }

}
