package hu.bme.librexandra.service;

import hu.bme.librexandra.entity.User;
import hu.bme.librexandra.model.UserModel;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface UserService {

    User create(UserModel userModel);

    User createFirst(UserModel userModel);

    List<User> listUsers();

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void setStatus(Long id, User.Status status);

    long count();

}
