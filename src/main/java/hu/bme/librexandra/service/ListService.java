package hu.bme.librexandra.service;

import hu.bme.librexandra.entity.BookList;
import hu.bme.librexandra.model.ListModel;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface ListService {

    int PUBLIC_LIST_LIMIT = 3;

    BookList create(ListModel listModel);

    BookList findById(Long id);

    @PreAuthorize("hasRole('ROLE_USER')")
    List<BookList> listByCurrentUser();

    @PreAuthorize("hasRole('ROLE_USER')")
    List<BookList> listByCurrentUserForBook(Long bookId);

    List<BookList> listForBook(Long bookId);

    @PreAuthorize("hasRole('ROLE_USER')")
    void deleteById(Long id);

    @PreAuthorize("hasRole('ROLE_USER')")
    void addEntry(Long id, Long bookId);

    @PreAuthorize("hasRole('ROLE_USER')")
    void removeEntry(Long id, Long bookId);

}
