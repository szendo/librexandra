package hu.bme.librexandra.repository;

import hu.bme.librexandra.entity.Book;
import hu.bme.librexandra.entity.BookList;
import hu.bme.librexandra.entity.BookListEntry;
import hu.bme.librexandra.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ListRepository extends JpaRepository<BookList, Long> {

    List<BookList> findByUserOrderByTimestampAsc(User user);

    @Query("select e from BookListEntry e where e.list = :list and e.book = :book")
    BookListEntry findEntryInList(@Param("list") BookList list, @Param("book") Book book);

    @Query("select l from BookList l join l.entries e where e.book = :book order by l.timestamp desc")
    List<BookList> findForBook(@Param("book") Book book);

}
