package hu.bme.librexandra.repository;

import hu.bme.librexandra.entity.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RatingRepository extends JpaRepository<Rating, Long> {

    @Query("select avg(r.value) from Rating r where r.book.id = :bookId")
    Double getBookScore(@Param("bookId") Long bookId);

    @Query("select r from Rating r where r.book.id = :bookId and r.user.id = :userId")
    Rating getRating(@Param("bookId") Long bookId, @Param("userId") Long userId);
}
