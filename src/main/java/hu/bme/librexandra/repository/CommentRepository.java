package hu.bme.librexandra.repository;


import hu.bme.librexandra.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query("select c from Comment c where c.status = hu.bme.librexandra.entity.Comment.Status.REPORTED")
    List<Comment> getReportedComments();

    @Modifying
    @Transactional
    @Query("delete from Comment c where c.id = :id")
    void deleteComment(@Param("id") Long id);

    @Query("select count(c.id) from Comment c where c.status = hu.bme.librexandra.entity.Comment.Status.REPORTED")
    long countReported();

}
