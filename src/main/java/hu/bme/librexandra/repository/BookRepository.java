package hu.bme.librexandra.repository;

import hu.bme.librexandra.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {

    List<Book> findByStatusOrderByTitleAsc(Book.Status status);

    Page<Book> findByStatusOrderByTitleAsc(Book.Status status, Pageable pageable);

    @Query("select b.cover from Book b where b.id = :id")
    byte[] loadCoverById(@Param("id") Long id);

    @Query("select count(b.id) from Book b where b.status = hu.bme.librexandra.entity.Book.Status.PENDING")
    long countPending();

}
