package hu.bme.librexandra.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Book {

    public static enum Status {
        ACCEPTED, PENDING
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @Column(length = 200)
    private String author;

    @Basic(optional = false)
    @Column(length = 200)
    private String title;

    @Basic(optional = false)
    @Column
    private short year;

    @Basic(optional = false)
    @Lob
    @Column
    private byte[] cover;

    @Basic(optional = false)
    @Lob
    @Column(length = 2000)
    private String description;

    @OneToMany(mappedBy = "book")
    private List<Comment> comments = new ArrayList<>();

    @OneToMany(mappedBy = "book")
    private List<Rating> ratings = new ArrayList<>();

    @Basic(optional = false)
    @Enumerated(value = EnumType.STRING)
    @Column
    private Status status;

    @OneToMany(mappedBy = "book")
    private List<BookListEntry> entries;

    @Transient
    private double score;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public short getYear() {
        return year;
    }

    public void setYear(short year) {
        this.year = year;
    }

    public byte[] getCover() {
        return cover;
    }

    public void setCover(byte[] cover) {
        this.cover = cover;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void addComment(Comment comment) {
        this.comments.add(comment);
        if (comment.getBook() != this) {
            comment.setBook(this);
        }
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public void addRating(Rating rating) {
        this.ratings.add(rating);
        if (rating.getBook() != this) {
            rating.setBook(this);
        }
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<BookListEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<BookListEntry> entries) {
        this.entries = entries;
    }

    public void addEntry(BookListEntry entry) {
        this.entries.add(entry);
        if (entry.getBook() == null || !this.getId().equals(entry.getBook().getId())) {
            entry.setBook(this);
        }
    }

    public void removeEntry(BookListEntry entry) {
        this.entries.remove(entry);
        if (entry.getBook() != null && this.getId().equals(entry.getBook().getId())) {
            entry.setBook(null);
        }
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

}
