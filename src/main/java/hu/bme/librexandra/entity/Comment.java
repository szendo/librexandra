package hu.bme.librexandra.entity;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Comment {

    public static enum Status {
        ORIGINAL, REPORTED, EDITED, VERIFIED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn
    private User user;

    @ManyToOne(optional = false)
    @JoinColumn
    private Book book;

    @Basic(optional = false)
    @Column
    private Instant timestamp;

    @Basic(optional = false)
    @Lob
    @Column
    private String text;

    @Basic(optional = false)
    @Enumerated(value = EnumType.STRING)
    @Column
    private Status status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
        if (!book.getComments().contains(this)) {
            book.getComments().add(this);
        }
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
