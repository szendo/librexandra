package hu.bme.librexandra.entity;

import javax.persistence.*;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

@Entity
public class BookList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn
    private User user;

    @Basic(optional = false)
    @Column
    private Instant timestamp;

    @Basic(optional = false)
    @Column
    private String name;

    @OneToMany(mappedBy = "list", orphanRemoval = true)
    private List<BookListEntry> entries;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BookListEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<BookListEntry> entries) {
        this.entries = entries;
    }

    public void addEntry(BookListEntry entry) {
        this.entries.add(entry);
        if (entry.getList() == null || !this.getId().equals(entry.getList().getId())) {
            entry.setList(this);
        }
    }

    public void removeEntry(BookListEntry entry) {
        this.entries.remove(entry);
        if (entry.getList() != null && this.getId().equals(entry.getList().getId())) {
            entry.setList(null);
        }
    }

}
