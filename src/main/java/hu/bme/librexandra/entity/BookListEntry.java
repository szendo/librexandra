package hu.bme.librexandra.entity;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class BookListEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic(optional = false)
    @Column
    private Instant timestamp;

    @ManyToOne(optional = false)
    @JoinColumn
    private BookList list;

    @ManyToOne(optional = false)
    @JoinColumn
    private Book book;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public BookList getList() {
        return list;
    }

    public void setList(BookList bookList) {
        this.list = bookList;
        if (bookList != null && !bookList.getEntries().contains(this)) {
            bookList.addEntry(this);
        }
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookListEntry that = (BookListEntry) o;
        return !(id != null ? !id.equals(that.id) : that.id != null);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

}
