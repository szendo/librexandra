package hu.bme.librexandra.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping({"/", "/index"})
public class IndexController extends ControllerBase {

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "index";
    }

}
