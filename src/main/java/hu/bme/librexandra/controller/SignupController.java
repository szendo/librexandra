package hu.bme.librexandra.controller;

import hu.bme.librexandra.entity.User;
import hu.bme.librexandra.model.UserModel;
import hu.bme.librexandra.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/signup")
public class SignupController extends ControllerBase {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String showSignupForm(@ModelAttribute("user") UserModel userModel) {
        return "signup";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String processSignup(@ModelAttribute("user") UserModel userModel) {
        final User user = userService.create(userModel);
        logger.info("User '{}' signed up with id {}", user.getUsername(), user.getId());
        return "redirect:/";
    }

}
