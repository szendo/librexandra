package hu.bme.librexandra.controller;

import hu.bme.librexandra.model.BookModel;
import hu.bme.librexandra.model.CommentModel;
import hu.bme.librexandra.service.BookService;
import hu.bme.librexandra.service.ListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;
import java.time.Instant;

@Controller
@RequestMapping("/books")
public class BookController extends ControllerBase {

    @Autowired
    private BookService bookService;

    @Autowired
    private ListService listService;

    @Autowired
    private ResourceLoader resourceLoader;

    @RequestMapping(method = RequestMethod.GET)
    public String listBooks(ModelMap model, @RequestParam(value = "page", defaultValue = "0") int page, @RequestParam(value = "size", defaultValue = "10") int size) {
        //model.put("books", bookService.listAccepted());
        model.put("booksPage", bookService.listAccepted(page, size));
        return "books/list";
    }


    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String showNewBookForm(@ModelAttribute("book") BookModel bookModel) {
        return "books/new";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String processNewBook(@ModelAttribute("book") BookModel bookModel) {
        bookService.create(bookModel);
        return "redirect:/books";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getBookById(@ModelAttribute("comment") CommentModel commentModel,
                              @PathVariable("id") Long id, ModelMap model, Principal principal) {
        model.put("book", bookService.findById(id));
        model.put("publicLists", listService.listForBook(id));
        if (principal != null) {
            model.put("lists", listService.listByCurrentUserForBook(id));
        }
        return "books/book";
    }

    @RequestMapping(value = "/{id}/addToList", method = RequestMethod.POST)
    public String addToList(@PathVariable("id") Long id, @RequestParam("listId") Long listId) {
        listService.addEntry(listId, id);
        return "redirect:/lists/" + listId;
    }

    @RequestMapping(value = "/{id}/cover.png", method = RequestMethod.GET, produces = "image/png")
    @ResponseBody
    public byte[] getBookCoverById(@PathVariable("id") Long id) {
        final byte[] cover = bookService.getCoverById(id);
        if (cover == null) {
            try {
                return StreamUtils.copyToByteArray(resourceLoader.getResource("classpath:/missing.png").getInputStream());
            } catch (IOException e) {
                logger.error("Error loading placeholder for missing cover", e);
                throw new RuntimeException("Error loading placeholder for missing cover", e);
            }
        }
        return cover;
    }

    @RequestMapping(value = "/comment/{id}", method = RequestMethod.POST)
    public String commentBookById(@PathVariable("id") Long id, @ModelAttribute("comment") CommentModel commentModel) {
        commentModel.setTimestamp(Instant.now());
        bookService.addComent(id, commentModel);
        return String.format("redirect:/books/%d", id);
    }

    @RequestMapping(value = "/rate/{id}", method = RequestMethod.POST)
    public String rateBookById(@PathVariable("id") Long id, @RequestParam("value") Short value) {
        bookService.rateBook(id, value);
        return String.format("redirect:/books/%d", id);
    }


}
