package hu.bme.librexandra.controller;

import hu.bme.librexandra.entity.BookList;
import hu.bme.librexandra.entity.User;
import hu.bme.librexandra.model.ListModel;
import hu.bme.librexandra.service.ListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/lists")
public class ListController extends ControllerBase {

    @Autowired
    private ListService listService;

    @RequestMapping(method = RequestMethod.GET)
    public String listLists(ModelMap model, @ModelAttribute("list") ListModel list) {
        model.put("lists", listService.listByCurrentUser());
        return "lists/list";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String processNewList(@ModelAttribute("list") ListModel list) {
        listService.create(list);
        return "redirect:/lists";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String showListById(@PathVariable("id") Long id, ModelMap model, Authentication authentication) {
        final BookList bookList = listService.findById(id);
        model.put("list", bookList);
        if (authentication != null
                && ((User) authentication.getPrincipal()).getId().equals(bookList.getUser().getId())) {
            return "lists/showOwn";
        }
        return "lists/showPublic";
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.POST)
    public String deleteListById(@PathVariable("id") Long id) {
        listService.deleteById(id);
        return "redirect:/lists";
    }

    @RequestMapping(value = "/{id}/addBook/{bookId}", method = RequestMethod.POST)
    public String addEntryByBookId(@PathVariable("id") Long id, @PathVariable("bookId") Long bookId) {
        listService.addEntry(id, bookId);
        return "redirect:/lists/" + id;
    }

    @RequestMapping(value = "/{id}/removeBook/{bookId}", method = RequestMethod.POST)
    public String removeEntryByBookId(@PathVariable("id") Long id, @PathVariable("bookId") Long bookId) {
        listService.removeEntry(id, bookId);
        return "redirect:/lists/" + id;
    }

}
