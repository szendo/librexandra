package hu.bme.librexandra.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
@RequestMapping("/login")
public class LoginController extends ControllerBase {

    @RequestMapping(method = RequestMethod.GET)
    public String login(@RequestParam Optional<String> reason, ModelMap modelMap) {
        modelMap.put("reason", reason.orElse("user"));
        return "login";
    }

}
