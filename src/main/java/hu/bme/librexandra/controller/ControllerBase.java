package hu.bme.librexandra.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

public abstract class ControllerBase {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(AccessDeniedException.class)
    public String accessDeniedHandler(WebRequest request) {
        logger.warn("Access denied: {}", request.getDescription(true));
        return "redirect:/login?reason=require-auth";
    }

}
