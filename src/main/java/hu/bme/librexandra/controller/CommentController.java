package hu.bme.librexandra.controller;

import hu.bme.librexandra.entity.Comment;
import hu.bme.librexandra.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/comments")
public class CommentController extends ControllerBase {

    @Autowired
    CommentService commentService;

    @RequestMapping(value = "report/{id}", method = RequestMethod.POST)
    public String reportComment(@PathVariable("id") Long id) {
        Comment comment = commentService.findById(id);
        commentService.reportComment(id);
        return String.format("redirect:/books/%d", comment.getBook().getId()) ;
    }
}
