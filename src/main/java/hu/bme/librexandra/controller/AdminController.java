package hu.bme.librexandra.controller;

import hu.bme.librexandra.entity.Book;
import hu.bme.librexandra.entity.Comment;
import hu.bme.librexandra.entity.User;
import hu.bme.librexandra.exception.ApplicationAlreadyConfiguredException;
import hu.bme.librexandra.model.UserModel;
import hu.bme.librexandra.service.BookService;
import hu.bme.librexandra.service.CommentService;
import hu.bme.librexandra.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

@Controller
@RequestMapping("/admin")
public class AdminController extends ControllerBase {

    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/config", method = RequestMethod.GET)
    public String showInitialConfigForm(@ModelAttribute("user") UserModel userModel, WebRequest webRequest) {
        if (userService.count() > 0) {
            logger.warn("Application already configured: {}", webRequest);
            throw new ApplicationAlreadyConfiguredException();
        } else {
            return "admin/config";
        }
    }

    @RequestMapping(value = "/config", method = RequestMethod.POST)
    public String processInitialConfig(@ModelAttribute("user") UserModel userModel, WebRequest webRequest) {
        if (userService.count() > 0) {
            logger.warn("Application already configured: {}", webRequest);
            throw new ApplicationAlreadyConfiguredException();
        } else {
            userService.createFirst(userModel);
            return "redirect:/";
        }
    }

    @RequestMapping(value = "/approveBook", method = RequestMethod.GET)
    public String listPendingBooks(ModelMap model) {
        model.put("books", bookService.listPending());
        return "admin/approveBook";
    }

    @RequestMapping(value = "/approveBook/{id}", method = RequestMethod.POST)
    public String approveBookById(@PathVariable("id") Long id) {
        bookService.setStatus(id, Book.Status.ACCEPTED);
        return "redirect:/admin/approveBook";
    }

    @RequestMapping(value = "/deleteBook/{id}", method = RequestMethod.POST)
    public String deleteBookById(@PathVariable("id") Long id) {
        bookService.delete(id);
        return "redirect:/admin/approveBook";
    }

    @RequestMapping(value = "/manageUsers", method = RequestMethod.GET)
    public String listUsers(ModelMap model) {
        model.put("users", userService.listUsers());
        return "admin/manageUsers";
    }

    @RequestMapping(value = "/promoteUser/{id}", method = RequestMethod.POST)
    public String promoteUser(@PathVariable("id") Long id) {
        userService.setStatus(id, User.Status.ADMIN);
        return "redirect:/admin/manageUsers";
    }

    @RequestMapping(value = "/demoteUser/{id}", method = RequestMethod.POST)
    public String demoteUser(@PathVariable("id") Long id) {
        userService.setStatus(id, User.Status.USER);
        return "redirect:/admin/manageUsers";
    }

    @RequestMapping(value = "/banUser/{id}", method = RequestMethod.POST)
    public String banUser(@PathVariable("id") Long id) {
        userService.setStatus(id, User.Status.BANNED);
        return "redirect:/admin/manageUsers";
    }

    @RequestMapping(value = "/unbanUser/{id}", method = RequestMethod.POST)
    public String unbanUser(@PathVariable("id") Long id) {
        userService.setStatus(id, User.Status.USER);
        return "redirect:/admin/manageUsers";
    }

    @RequestMapping(value = "/manageComments", method = RequestMethod.GET)
    public String listReportedComment(ModelMap model) {
        model.put("comments", commentService.getReportedComments());
        return "/admin/manageComments";
    }

    @RequestMapping(value = "/editComment/{id}", method = RequestMethod.POST)
    public String editComment(@PathVariable("id") Long id, @RequestParam("text") String text) {
        commentService.editComment(id, text);
        return "redirect:/admin/manageComments";
    }

    @RequestMapping(value = "/verifyComment/{id}", method = RequestMethod.POST)
    public String verifyComment(@PathVariable("id") Long id) {
        commentService.setStatus(id, Comment.Status.VERIFIED);
        return "redirect:/admin/manageComments";
    }

    @RequestMapping(value = "/deleteComment/{id}", method = RequestMethod.POST)
    public String deleteComment(@PathVariable("id") Long id) {
        commentService.deleteComment(id);
        return "redirect:/admin/manageComments";
    }

}
