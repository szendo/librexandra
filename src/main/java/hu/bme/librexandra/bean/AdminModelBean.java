package hu.bme.librexandra.bean;

import hu.bme.librexandra.service.BookService;
import hu.bme.librexandra.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AdminModelBean {

    @Autowired
    private BookService bookService;

    @Autowired
    private CommentService commentService;

    public long getPendingCount() {
        return bookService.countPending();
    }

    public long getReportedCount() {
        return commentService.countReported();
    }

}
