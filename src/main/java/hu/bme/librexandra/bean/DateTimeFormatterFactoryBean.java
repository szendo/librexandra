package hu.bme.librexandra.bean;

import org.springframework.beans.factory.FactoryBean;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateTimeFormatterFactoryBean implements FactoryBean<DateTimeFormatter> {

    @Override
    public DateTimeFormatter getObject() throws Exception {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
                .withZone(ZoneId.systemDefault());
    }

    @Override
    public Class<?> getObjectType() {
        return DateTimeFormatter.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

}
