<#include '../_template.html.ftl'>

<@page title='könyvek jóváhagyása' active='approveBooks'>

<table>
    <thead>
    <tr>
        <th width="165">Borító</th>
        <th width="200">Cím</th>
        <th width="200">Szerző</th>
        <th width="100">Kiadás éve</th>
        <th width="70">Művelet</th>
    </tr>
    </thead>
    <tbody>
        <#if books?size &gt; 0>
            <#list books as b>
            <tr>
                <td><img src="<@spring.url '/books/${b.id}/cover.png'/>"></td>
                <td><a href="/books/${b.id}">${b.title}</a></td>
                <td>${b.author}</td>
                <td>${b.year?c}</td>
                <td>
                    <form method="post">
                        <ul class="button-group stack">
                            <li>
                                <button class="small button success"
                                        formaction="<@spring.url '/admin/approveBook/' + b.id/>">
                                    <i class="fa fa-fw fa-check"></i>
                                </button>
                            </li>
                            <li>
                                <button class="small button alert"
                                        formaction="<@spring.url '/admin/deleteBook/' + b.id/>">
                                    <i class="fa fa-fw fa-trash"></i>
                                </button>
                            </li>
                        </ul>
                    </form>
                </td>
            </tr>
            </#list>
        <#else>
        <tr>
            <td colspan="5">Nincs jóváhagyásra váró könyv!</td>
        </tr>
        </#if>
    </tbody>
</table>

</@page>