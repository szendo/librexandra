<#include '../_template.html.ftl'>
<@page title='felhasználók' active='users'>

<table>
    <thead>
    <tr>
        <th width="200">Felhasználónév</th>
        <th width="200">Könyv</th>
        <th width="400">Komment</th>
        <th width="100">Műveletek</th>
    </tr>
    </thead>
    <tbody>
        <#list comments as c>
        <tr>
            <td>${c.user.username}</td>
            <td>${c.book.title}</td>
            <td>
                <label>
                    <input name="text" value="${c.text}" form="form-${c_index}">
                </label>
            </td>
            <td>
                <form id="form-${c_index}" method="post">
                    <ul class="button-group stack">
                        <li>
                            <button formaction="<@spring.url '/admin/verifyComment/${c.id}'/>" class="small success">
                                <i class="fa fa-fw fa-check"></i>
                            </button>
                        </li>
                        <li>
                            <button formaction="<@spring.url '/admin/editComment/${c.id}'/>" class="small warning">
                                <i class="fa fa-fw fa-pencil-square-o"></i>
                            </button>
                        </li>
                        <li>
                            <button formaction="<@spring.url '/admin/deleteComment/${c.id}'/>" class="small alert">
                                <i class="fa fa-fw fa-trash"></i>
                            </button>
                        </li>
                    </ul>
                </form>
            </td>
        </tr>
        </#list>
    </tbody>
</table>

</@page>