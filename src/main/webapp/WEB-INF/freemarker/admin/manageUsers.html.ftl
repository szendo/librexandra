<#include '../_template.html.ftl'>
<@page title='felhasználók' active='users'>

<table>
    <thead>
    <tr>
        <th width="200">Felhasználónév</th>
        <th width="200">E-mail</th>
        <th width="400">Cselekvések</th>
    </tr>
    </thead>
    <tbody>
        <#list users as u>
        <tr>
            <td>${u.username}</td>
            <td>${u.email}</td>
            <td>
                <#if u.status=="USER">
                    <form action="<@spring.url '/admin/promoteUser/' + u.id/>" method="post">
                        <button class="tiny">Adminjog adása</button>
                    </form>
                </#if>
                <#if u.status=="ADMIN">
                    <form action="<@spring.url '/admin/demoteUser/' + u.id/>" method="post">
                        <button class="tiny">Adminjog visszavonása</button>
                    </form>
                </#if>
                <#if u.status!="BANNED">
                    <form action="<@spring.url '/admin/banUser/' + u.id/>" method="post">
                        <button class="tiny">Kitiltás</button>
                    </form>
                </#if>
                <#if u.status=="BANNED">
                    <form action="<@spring.url '/admin/unbanUser/' + u.id/>" method="post">
                        <button class="tiny">Kitiltás visszavonása</button>
                    </form>
                </#if>
            </td>
        </tr>
        </#list>
    </tbody>
</table>

</@page>