<#include '../_template.html.ftl'>

<@page title='konfiguráció' active='signup'>
<form action="" method="post">
    <div class="row">
        <div class="large-6 push-3 columns">
            <label>Felhasználónév
                <@spring.formInput 'user.username' 'required autofocus'/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="large-6 push-3 columns">
            <label>Jelszó
                <@spring.formPasswordInput 'user.password' 'prequired'/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="large-6 push-3 columns">
            <label>E-mail
                <@spring.formInput 'user.email' 'required' 'email'/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="large-6 push-3 columns">
            <button class="expand">Regisztráció</button>
        </div>
    </div>
</form>
</@page>