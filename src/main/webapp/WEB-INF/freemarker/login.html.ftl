<#include '_template.html.ftl'>

<@page title='bejelentkezés' active='login'>

    <#switch reason>
        <#case 'bad-credentials'>
        <div data-alert class="alert-box alert">
            Hibás felhasználónév/jelszó!
            <a href="#" class="close">&times;</a>
        </div>
            <#break>
        <#case 'require-auth'>
        <div data-alert class="alert-box warning">
            A művelet végrehajtásához bejelentkezés szükséges!
            <a href="#" class="close">&times;</a>
        </div>
            <#break>
    </#switch>

<form action="<@spring.url '/doLogin'/>" method="post">
    <div class="row">
        <div class="large-6 push-3 columns">
            <label>Felhasználónév
                <input type="text" name="j_username" autofocus/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="large-6 push-3 columns">
            <label>Jelszó
                <input type="password" name="j_password"/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="large-6 push-3 columns">
            <button class="expand">Bejelentkezés</button>
        </div>
    </div>
</form>
</@page>

