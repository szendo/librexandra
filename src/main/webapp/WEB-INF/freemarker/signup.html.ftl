<#include '_template.html.ftl'>

<@page title='regisztráció' active='signup'>
<form action="" method="post">
    <div class="row">
        <div class="large-6 push-3 columns">
            <label>Felhasználónév
                <@spring.formInput 'user.username' 'required autofocus maxlength="200"'/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="large-6 push-3 columns">
            <label>Jelszó
                <@spring.formPasswordInput 'user.password' 'required'/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="large-6 push-3 columns">
            <label>E-mail
                <@spring.formInput 'user.email' 'required maxlength="200"' 'email'/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="large-6 push-3 columns">
            <button class="expand">Regisztráció</button>
        </div>
    </div>
</form>
</@page>