<#import '/spring.ftl' as spring>
<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />

<@security.authorize ifAnyGranted='ROLE_USER' var='isLoggedIn'/>
<@security.authorize ifAnyGranted='ROLE_ADMIN' var='isAdmin'/>

<#assign menuItems=[
{'name':'', 'url': '/', 'title':'Főoldal'},
{'name':'books', 'url': '/books', 'title':'Könyvek'},
{'name':'lists', 'url': '/lists', 'title':'Listák'}
] />

<#macro page title='' active=''>
<!DOCTYPE html>
<html class="no-js">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Librexandra<#if title?has_content> &rsaquo; ${title}</#if></title>
    <link rel="stylesheet" href="<@spring.url '/res/css/normalize.css'/>">
    <link rel="stylesheet" href="<@spring.url '/res/css/foundation.css'/>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="<@spring.url '/res/js/vendor/modernizr.js'/>"></script>
</head>
<body>
<div class="row">
    <div class="large-3 columns">
        <h1>Librexandra</h1>
    </div>
    <div class="large-9 columns">
        <ul class="inline-list right">
            <#if isLoggedIn>
                <li><@security.authentication property='principal.username'/></li>
                <li><a href="<@spring.url '/doLogout'/>">Kijelentkezés</a></li>
            <#else >
                <li><a href="<@spring.url '/signup'/>">Regisztráció</a></li>
                <li><a href="<@spring.url '/login'/>">Bejelentkezés</a></li>
            </#if>
        </ul>
    </div>
</div>

<div class="row">
    <div class="large-9 push-3 columns">
        <#nested>
    </div>
    <div class="large-3 pull-9 columns">
        <ul class="side-nav">
            <#list menuItems as item>
                <li<#if item.name==active> class="active"</#if>>
                    <a href="<@spring.url item.url/>">${item.title}</a>
                </li>
            </#list>
            <#if isAdmin>
                <li class="divider"></li>
                <li>
                    <a href="<@spring.url '/admin/approveBook'/>">
                        Könyv jóváhagyás
                        <#if adminModelBean.pendingCount &gt; 0>
                        <span class="right alert round label">${adminModelBean.pendingCount}</span>
                        </#if>
                    </a>
                </li>
                <li>
                    <a href="<@spring.url '/admin/manageComments'/>">
                        Kommentek moderálása
                        <#if adminModelBean.reportedCount &gt; 0>
                            <span class="right alert round label">${adminModelBean.reportedCount}</span>
                        </#if>
                    </a>
                </li>
                <li>
                    <a href="<@spring.url '/admin/manageUsers'/>">
                        Felhasználók kezelése
                    </a>
                </li>
            </#if>
        </ul>
    </div>
</div>


<script src="<@spring.url '/res/js/vendor/jquery.js'/>"></script>
<script src="<@spring.url '/res/js/foundation.min.js'/>"></script>
<script>
    $(document).foundation();
</script>
</body>
</html>
</#macro>

