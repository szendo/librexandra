<#include '../_template.html.ftl'>
<@page title='könyv' active='book'>

<ul class="pricing-table">
    <li class="price">${book.title} (${book.year?c})</li>

    <div class="row">
        <div class="large-6 columns left"><img src="<@spring.url '/books/${book.id}/cover.png'/>"/></div>
        <div class="large-6 columns">
            <li class="bullet-item">${book.author}</li>
            <li class="bullet-item">${book.score}/10</li>
            <li class="bullet-item">${book.description}</li>
            <#if isLoggedIn>
                <li class="bullet-item">
                    <form action="<@spring.url '/books/rate/${book.id}'/>" method="post">
                        <div class="row">
                            <div class>
                                <label>Érték
                                    <select name="value">
                                        <#list 1..10 as v>
                                            <option class="tiny" value=${v}>${v}</option>
                                        </#list>
                                    </select>
                                </label>
                            </div>
                            <div class="row">
                                <div>
                                    <button class="tiny">Értékelés</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </li>
            </#if>
            <li class="bullet-item">
                <#if publicLists?size &gt; 0>
                    Az alábbi listákon szerepel:
                    <ul style="font-size: 0.85rem">
                        <#list publicLists as l>
                            <li>
                                <a href="<@spring.url '/lists/${l.id}'/>">${l.name}</a>
                                (by ${l.user.username})
                            </li>
                        </#list>
                    </ul>
                <#else>
                    A könyv még nincs egy listán sem.
                </#if>
            </li>
        </div>
    </div>
</ul>

    <#if (lists![])?size &gt; 0>
    <form action="<@spring.url '/books/${book.id}/addToList'/>" method="post">
        <div class="row">
            <div class="large-6 push-3 columns">
                <label>Listák
                    <select name="listId">
                        <#list lists as l>
                            <option value="${l.id}">${l.name}</option>
                        </#list>
                    </select>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="large-6 push-3 columns">
                <button class="expand">Hozzáadás</button>
            </div>
        </div>
    </form>
    </#if>

    <#list book.comments as c>
    <div style="padding: 0.5em<#if c_index % 2 != 0>; background-color: #ddd</#if>">
        <div class="row">
            <div class="large-6 columns" style="padding-bottom: 0.5em">
                <i class="fa fa-user"></i>
            ${c.user.username} írta:
            </div>
            <div class="large-4 columns">
                <i class="fa fa-clock-o"></i>
            ${formatter.format(c.timestamp)}
            </div>
            <div class="large-2 columns">
                <#switch c.status.name()>
                    <#case 'ORIGINAL'>
                        <#if isLoggedIn>
                            <form action="<@spring.url '/comments/report/${c.id}'/>" method="post">
                                <button class="tiny" style="margin-bottom: 0">Jelentés</button>
                            </form>
                        </#if>
                        <#break>
                    <#case 'REPORTED'>
                        jelentve
                        <#break>
                    <#case 'EDITED'>
                        szerkesztve
                        <#break>
                    <#case 'VERIFIED'>
                        jóváhagyva
                        <#break>
                </#switch>
            </div>
        </div>
        <div class="row" style="word-break: break-word">
            <div class="large-12 columns">
                <#if c.status.name() != 'REPORTED'>
                    <p>${c.text}</p>
                <#else >
                    <p>[jelentve, moderálás alatt]</p>
                </#if>
            </div>
        </div>
    </div>
    </#list>
    <#if isLoggedIn>
    <div class="row">
        <div class="large-6 push-3 columns">
            <form action="<@spring.url '/books/comment/${book.id}'/>" method="post">
                Komment írása:
                <@spring.formInput 'comment.text' 'required' />
                <div class="large-6 push-3 columns">
                    <button class="expand">Elküldés</button>
                </div>
            </form>
        </div>
    </div>
    <#else>
    <div class="row">
        <div class="large-6 push-3 columns">
            Komment írásához be kell jelenkezned!
        </div>
    </div>
    </#if>
</@page>
