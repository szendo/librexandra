<#include '../_template.html.ftl'>

<@page title='új könyv' active='books'>
<form action="" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="large-6 push-3 columns">
            <label>Cím
                <@spring.formInput 'book.title' 'required autofocus maxlength="200"'/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="large-6 push-3 columns">
            <label>Szerző
                <@spring.formInput 'book.author' 'required maxlength="200"'/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="large-6 push-3 columns">
            <label>Leírás
                <@spring.formTextarea 'book.description' 'required maxlength="2000"'/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="large-6 push-3 columns">
            <label>Kiadás éve
                <@spring.formInput 'book.year' 'required min="-9999" max="9999"' 'number'/>
            </label>
        </div>
    </div>
       <div class="row">
        <div class="large-6 push-3 columns">
            <label>Borítókép (max 1 MB)
                <input type="file" name="cover" required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="large-6 push-3 columns">
            <button class="expand">Elküldés</button>
        </div>
    </div>
</form>
</@page>