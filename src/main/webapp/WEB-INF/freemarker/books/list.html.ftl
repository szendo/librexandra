<#include '../_template.html.ftl'>

<@page title='könyvek' active='books'>

<div class="row">
    <div class="large-3 push-9 columns">
        <a class="button small expand" href="<@spring.url '/books/new'/>">Új könyv felvétele</a>
    </div>
</div>

<table>
    <thead>
    <tr>
        <th width="200">Cím</th>
        <th width="200">Szerző</th>
        <th width="100">Kiadás éve</th>
    </tr>
    </thead>
    <tbody>
        <#list booksPage.content as b>
        <tr>
            <td><a href="<@spring.url '/books/${b.id}'/>">${b.title}</a></td>
            <td>${b.author}</td>
            <td>${b.year?c}</td>
        </tr>
        </#list>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="3">
            <#if booksPage.totalPages &gt; 0>
                <#assign hasPrev=booksPage.hasPrevious()/>
                <#assign hasNext=booksPage.hasNext()/>
                <ul class="pagination" style="margin-bottom:0; display: flex; justify-content: center">
                    <li class="arrow<#if !hasPrev> unavailable</#if>">
                        <a<#if hasPrev> href="?page=${booksPage.number-1}"</#if>>
                            <i class="fa fa-chevron-circle-left"></i>
                        </a>
                    </li>
                    <#assign ellip=false/>
                    <#list 1..booksPage.totalPages as p>
                        <#if ( p &lt; 4 || (p - booksPage.number - 1)?abs &lt; 2 || p &gt; booksPage.totalPages - 3 )>
                            <#assign ellip=true/>
                            <li<#if booksPage.number == p-1> class="current"</#if>><a href="?page=${p-1}">${p}</a></li>
                        <#elseif ellip>
                            <#assign ellip=false/>
                            <li class="arrow unavailable"><a><i class="fa fa-ellipsis-h"></i></a></li>
                        </#if>
                    </#list>
                    <li class="arrow<#if !hasNext> unavailable</#if>">
                        <a<#if hasNext> href="?page=${booksPage.number+1}"</#if>>
                            <i class="fa fa-chevron-circle-right"></i>
                        </a>
                    </li>
                </ul>
            </#if>
        </td>
    </tr>
    </tfoot>
</table>

</@page>