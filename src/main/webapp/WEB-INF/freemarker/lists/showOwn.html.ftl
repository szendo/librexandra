<#include '../_template.html.ftl'>

<@page title='listák' active='lists'>

<h3>${list.name}</h3>

<form action="<@spring.url '/lists/${list.id}/delete'/>" method="post">
    <button class="alert">törlés!</button>
</form>

<form action="" method="post">
    <ol>
        <#list list.entries as e>
            <li>${e.book.title}</li>
            <button formaction="<@spring.url '/lists/${list.id}/removeBook/${e.book.id}'/>">×</button>
        </#list>
    </ol>
</form>

<p id="share"></p>
<script>
    (function () {
        var url = document.location.href;
        var link = document.createElement("a");
        link.appendChild(document.createTextNode(url));
        link.href = url;
        var shareDiv = document.getElementById('share');
        shareDiv.appendChild(document.createTextNode("A listát a következő URL segítségével oszthatod meg másokkal: "));
        shareDiv.appendChild(link);
    })();
</script>

</@page>