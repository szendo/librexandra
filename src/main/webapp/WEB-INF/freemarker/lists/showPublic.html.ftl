<#include '../_template.html.ftl'>

<@page title='${list.name} (by ${list.user.username})' active='lists'>

<h3>${list.name}</h3>

<form action="" method="post">
    <ol>
        <#list list.entries as e>
            <li>${e.book.title}</li>
        </#list>
    </ol>
</form>

</@page>