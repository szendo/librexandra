<#include '../_template.html.ftl'>

<@page title='listák' active='lists'>

<form action="" method="post">
    <div class="row">
        Új lista neve:
        <@spring.formInput 'list.name' 'required' />
    </div>
    <div class="row">
        <div class="large-6 push-3 columns">
            <button class="expand">Létrehozás</button>
        </div>
    </div>
</form>

<div class="row">
    <table>
        <thead>
        <tr>
            <th width="200">Név</th>
        </tr>
        </thead>
        <tbody>
            <#list lists as l>
            <tr>
                <td><a href="<@spring.url '/lists/${l.id}'/>">${l.name}</a></td>
            </tr>
            </#list>
        </tbody>
    </table>
</div>

</@page>